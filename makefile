CC = gcc
CFLAGS = -O2
DESTINST := /usr/bin/
#CFLAGS = -g -Wall

hmtk: html-bld.o html-lib.o ink-util.o
	$(CC) $(CFLAGS) html-bld.o html-lib.o ink-util.o -o hmtk

html-bld.o: src/main.c
	$(CC) -c $(CFLAGS) src/main.c -o html-bld.o

html-lib.o: src/ink-html.c
	$(CC) -c $(CFLAGS) src/inkhtml.c -o html-lib.o

ink-util.o: src/inkutil.c
	$(CC) -c $(CFLAGS) src/inkutil.c -o ink-util.o

clean:
	rm $(wildcard *.o)

install: $(DESTINST)/hmtk
	@echo "Installing"

local: /usr/local/bin/lhmtk
	@echo "Installing localy"

$(DESTINST)/hmtk: hmtk
	# mkdir -p /usr/local/hmtk
	cp ./hmtk $(DESTINST)


/usr/local/bin/lhmtk: hmtk
	# mkdir -p /usr/local/hmtk
	cp ./hmtk /usr/local/bin/lhmtk
