
# Ink HTML Manipulation Toolkit

## Description
A package of programs for server-side usage, made in c and cgi.

For now the only feature it has is linking blocks of HTML code into the HTML file requested by htttp


## Installation
### for server usage:
Clone/download the repository, change directory to where you saved it, and simpley run the make command
```code
# cd the/path/to/where/you/saved/the/files
# make
```

Optional: used to auto delete any .o file that may been made during the compilation)
```code
make clean 
```

Last thing you need to do is to configure your web server to call this program for every request that end with .html extension

## Usage
Inside your website's html files, whenever you want to insert a block, insert a line formated like this:
```code
<!--:INK <name>name-of-the-block.html</name> <pars>address=value, another-address=another-value</pars>-->
```
The <pars> tag is optional. You can use it to pars additional data(value) to the block, that would later be replaced with a place-holdesrs(address) within the block.
Like this:
```code
<!--:VAL <address/> --> <!--:VAL <address/> -->
```


## Roadmap
1. Combining the `main()` function and the `read_block()` function into one recurcive function.
2. Making pseudo random engine that generates pre-made content randomly, for the 'random sruff' section in my website.
3. Adding an option to generate and save static html pages so they could be loaded on client request without the need to run the program in 'real-time'.
4. Adding tools for hosting a blog-like website (tags-support, sort by date, etc).

## Contributing
DISCLAMER: I made this project mainly for my own learning and practicing purposes.
Therefore It won't be a wise idea to use it in production.
I am pretty sure there is a lot of place for improvement.
If you'd like to help me I'd really appreciate that.

I accept contribution, it would be better if you contact me before sending a pull request,
but eitherway I'd review your submission and decide if I want to add it to the project or not.

This program in its final form designed to work only with web-servers. If you still want to debug it without a web-server
you need to replace the get_env() calls with argv[] parameters and make sure that you make a valid path to a directory that mimic the use of /etc/html-blocks/ (or make one as root)

### Coding Style
I use tabs for indentations (used spece in the past so there might be some places it is still being used).
Curly brackets are always begun in a new line.

### Design decitions
1. The raw files the program use must keep regular html format.
In a way that if you'll try to load one of them in a browser they would be formated correctly and load fine
2. Even in a case of a raw file formated incorrectly (for example missing `-->` at the end of an `<!--:INK`).
The program must not crush! and just print an error messege to perror and ignore the poorly formated part.


## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Project status
This project is in active development. (kinda)
