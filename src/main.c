/* License Notice:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inkhtml.h"
#include "inkutil.h"

void option_error();

int main(int argc, char const *argv[])
{
	//oppening the appropriate file
	FILE *subject;
	char *configAddress;

	if ( (argc >= 2) && !strcmp(argv[1], "-s") && getenv("DOCUMENT_ROOT") )
	{
		configAddress = (char*)malloc((strlen(getenv("DOCUMENT_ROOT")) + 32) * sizeof(char));
		strcpy(configAddress, getenv("DOCUMENT_ROOT"));
		strcat(configAddress, "/");
		char *address = dal_get_html_path(getenv("DOCUMENT_ROOT"), getenv("SCRIPT_NAME"));
		subject = fopen(address, "r");
		free(address);
		printf("Content-type: text/html\n\r\n\r");
	}
	else if ( argc >= 3 )
	{
		if (!strcmp(argv[1], "-p"))
		{
			configAddress = (char*)malloc((strlen(getenv("PWD")) + 32) * sizeof(char));
			strcpy(configAddress, getenv("PWD"));
			strcat(configAddress, "/");
		}
		else if (!strcmp(argv[1], "-f"))
		{
		size_t dir_len = reverse_strstr(argv[2], '/');
			configAddress = (char*)malloc((dir_len + 32) * sizeof(char));
			strcpy(configAddress, argv[2]);
			configAddress[dir_len] = '\0';
		}
		else
		{
			option_error();
			return 1;
		}
		subject = fopen(argv[2], "r");
	}
	else
	{
		option_error();
		return 1;
	}

	if (subject == NULL)
	{
		perror("Error: subject couldn't be open \n");
			free(configAddress);
		return 2;
	}
	// end

	// setting up variables
	char *endOfPath; // point to the last character in configAddess
	endOfPath = &configAddress[strlen(configAddress)];
	int status = 0;
	status = read_file(subject, configAddress, endOfPath, NULL, "<!--:INK");
	fclose(subject);
	free(configAddress);
	return status;
}


void option_error()
{
	perror
	(
		"Need to specify an option for relative path: html-builder (option) path/\n\t-s For server-side usage,\n\t-f Relative to the file's path,\n\t-p Relative to power directory"
	);
}

