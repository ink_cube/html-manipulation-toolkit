#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inkhtml.h"
#include "inkutil.h"


void manipulate_block(parsdat* addresses, char* b_line)
{
	char* token;
	const char comment[] = "<!--:VAL";
	char* end_of_pars;
	char* comment_loc;
	char* start_of_parts;
	int lenss;

	if (addresses != NULL && strstr(b_line, comment))
	{
		token = b_line;
		for (size_t i = 0; addresses[i].address != NULL; ++i)
		{
			if
			(
				( start_of_parts = strstr(token, comment) ) != NULL &&
				( end_of_pars = strstr(start_of_parts, "-->")) != NULL &&
				(comment_loc = strstr(start_of_parts, addresses[i].address)) &&
				*(comment_loc - 1) == '<' &&
				*(comment_loc + strlen(addresses[i].address)) == '/' &&
				*(comment_loc + strlen(addresses[i].address) + 1) == '>'
			)
			{
				char* rest_of_line;
				rest_of_line = (char*)malloc
				(
					(strlen(end_of_pars) + 1) * sizeof(char)
				);
				strcpy(rest_of_line, (end_of_pars + 3));
				*start_of_parts = '\0';
				strcat(token, addresses[i].value);
				lenss = strlen(token) + 1;
				strcat(token, rest_of_line);
				token = &token[lenss];
				free(rest_of_line);
			}
		}
	}
}


int insert_block(char* line, char* configAddress, char* endOfPath)
{
	char *parsStart;
	char *parsEnd;

	char* localCA = (char*)malloc(strlen(configAddress) + 1);
	strcpy(localCA, configAddress);

	char *keyWordStart;
	char *keyWordEnd;

	const char parsWord[] = "<pars>";
	const char parsEndWord[] = "</pars>";

	int keyWordLen;

	const char keyWordIndic[] = "<name>";
	const char endOfWordIndic[] = "</name>";

	rm_wspaces(line);
	parsStart = strstr(line, parsWord);
	parsEnd = strstr(line, parsEndWord);
	if
	( // look for a file-name (block) to call to
		(keyWordStart = strstr(line, keyWordIndic)) &&
		(keyWordEnd = strstr(keyWordStart, endOfWordIndic))
	)
	{
		keyWordStart += strlen(keyWordIndic);
		*keyWordEnd = '\0';
		keyWordLen = keyWordEnd - keyWordStart;
		if (keyWordLen > 32)
		{
			perror("Buffer overflow: keyword lenth out of range");
			return 1;
		}
		else
		{
			strcat(configAddress, keyWordStart); // take the config.address (path) and append
																					 // the file name (relative path) to it, to generate
																					 // an absolute path off of it.
			FILE* fileptr = fopen(configAddress, "r");
			if (fileptr == NULL)
			{
				perror("Could not open the file-block! path: ");
			}
			else
			{
				if ( parsStart && parsEnd )
				{
					*parsEnd = '\0';
					parsdat* addresses = dal_duotok(parsStart + strlen(parsWord)); // 6 is the length of the string <pars>
					*endOfPath = '\0';
					read_file(fileptr, localCA, endOfPath, addresses, "<!--:INK");
					free(addresses);
				}
				else
				{
					*endOfPath = '\0';
					read_file(fileptr, localCA, endOfPath, NULL, "<!--:INK");
				}
				*endOfPath = '\0';
				fclose(fileptr);
			}
		}
	}
	free(localCA);
	return 0;
}


int read_file(FILE* subject, char* configAddress, char* endOfPath, parsdat* srcAddresses, const char lineIndic[])
{
	char *lineStart;
	const char endOfIndic[] = "-->";

	bool endOfLine = 1;
	char line[256];

	while (endOfLine != 0)
	{
		if (fgets(line, 256, subject) == NULL) /*reading lines from the requested html*/
		{
			endOfLine = 0;
		}
		else
		{
			// search for lineIndic in the current line to attach a block
			manipulate_block(srcAddresses, line);
			if (
				(lineStart = strstr(line, lineIndic)) &&
				strstr(lineStart, endOfIndic) &&
				configAddress && endOfPath
			)
			{
				if ( insert_block(line, configAddress, endOfPath) == 1 )
				{
					continue;
				}
      }
			else
			{
        printf("%s", line);
      }
		}
  }
	return 0;
}

/* This function return a dynamically allocated pointer
It is up to the caller to free it */
char* dal_get_html_path(const char* root_path, const char* html_name)
{
  char *req_path;
  int path_len = sizeof(char) * (strlen(html_name) + strlen(root_path) + 1);
  if ((req_path = malloc(path_len)) != NULL )
	{
    req_path[0] = '\0';
    strcat(req_path, root_path);
    strcat(req_path, html_name);
    return req_path;
  }
	else
	{
    perror("failed to allocate memory");
    return NULL;
  }
}

