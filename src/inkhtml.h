#ifndef INK_HMTK
#define INK_HMTK
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "inkutil.h"

void manipulate_block(parsdat* addresses, char* b_line);

int insert_block(char* line, char* configAddress, char* endOfPath);

int read_file(
		FILE* subject, char* configAddress, char* endOfPath, parsdat* srcAddresses, const char lineIndic[]
		);

char* dal_get_html_path(const char* root_path, const char* html_name);
#endif

