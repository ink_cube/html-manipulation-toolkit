#ifndef INK_UTIL
#define INK_UTIL
#include <stdlib.h>
#include <string.h>
typedef struct parsdat {
	char *address;
	char *value;
} parsdat;

size_t reverse_strstr(const char* str, char tav);

void rm_wspaces(char my_string[]);

parsdat* dal_duotok(char* my_string);

#endif

