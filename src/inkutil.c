#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "inkutil.h"
//typedef struct parsdat {
//	char *address;
//	char *value;
//} parsdat;


size_t reverse_strstr(const char* str, char tav)
{
	size_t i;
	for (i = strlen(str); str[i] != tav && i != 0; --i);
	if (i != 0) ++i ;
	return i;
}


void rm_wspaces(char my_string[])
{
	int string_indic = 0;
	int esc_indic = 0;
	int string_len = (strlen(my_string) + 1);
	for (int i = 0, g = 0; i < string_len; ++i)
	{
		if (my_string[i] == '\"' && esc_indic == 0)
		{
			string_indic++;
		}
		if
		(
			(
				(
					my_string[i] != ' ' && my_string[i] != '\t' &&
					my_string[i] != '\n' && my_string[i] != '\v' &&
					my_string[i] != '\f' && my_string[i] != '\r'
				)
				|| string_indic % 2 != 0 || my_string[i] == '\0'
			)
			&& ( esc_indic != 0 || my_string[i] != '\\' )
			&& !(esc_indic == 0 && my_string[i] == '\"')
		)
		{
			my_string[g] = my_string[i];
			g++;
		}
		if ( my_string[i] == '\\' ) 
		{
			esc_indic = 1;
		}
		else
		{
			esc_indic = 0;
		}
	}
}


parsdat* dal_duotok(char* my_string)
{
	if (my_string == NULL)
	{
		perror("no string has been parsed");
		return NULL;
	}
	parsdat* test = (parsdat*) calloc(2, sizeof(parsdat));
	if (test == NULL)
	{
		perror("failed to allocate memory");
		return NULL;
	}
	int str_size = strlen(my_string);
	test[0].address = &my_string[0]; //  pointing the first element in 'test'
																	 //  to point at the beginning of the string.
																	 //  later on we'll set \0 to cut it in the right place
	int string_indic = 0; // indicate whether the program reads text inside a \" or not.
	int format_indic = 0; // indicate wether the character we are are reading
												// is part of an addrees or part of a value, 0 = address 1 = value
	for (int i = 0, g = 0; i < str_size; ++i)
	{
		if (my_string[i] == '\"')
		{
			string_indic++;
		}
		if (string_indic % 2 == 0) // when the char is not inside a \":
		{
			if (my_string[i] == '=' && format_indic == 0)
			{
				++format_indic; // change mode to value reading from this char and farther
				my_string[i] = '\0'; // cut the first address in its end by replaceing the = with \0
				test[g].value = &my_string[i+1]; // giving element[g] a pointer to the charcter next to the =
			}
			else if	(my_string[i] == ',' && format_indic == 1 && i < (str_size-1))
			{
				--format_indic; // change mode to address reading from this char and farther
				my_string[i] = '\0'; // cut the first address in its end by replaceing the = with \0
				++g; // increament the amout of elements in test
				test = realloc(test, sizeof(parsdat)*(g + 2));
				if (test == NULL)
				{
					perror("failed to allocate memory");
					return NULL;
				}
				test[g].address	= &my_string[i+1]; // setting up the next element's address to be right after the ,
			}
		}
	}
	return test;
}
